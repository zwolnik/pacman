package pacman.conf;

import pacman.game.Board;

import java.awt.*;

/**
 *  Config class is responsible for holding static instances of all Game constants
 */
public class Config {
    public static final int GameSpeed = 10;
    public static final int DrawSpeed = 40;
    public static final int WWidth = 1000;
    public static final int WHeight = 1000;
    public static final int XOffset = 50;
    public static final int YOffset = 50;
    public static final int XUnit;
    public static final int YUnit;
    static {
        int BWidth = WWidth - 2*XOffset;
        int BHeight = WHeight - 2*YOffset;
        YUnit = BHeight / Board.template.length;
        XUnit = BWidth / Board.template[0].length();
    }

    public static final Color WallFg    = Color.decode("#111111");
    public static final Color WallBg    = Color.decode("#222222");
    public static final Color DefaultFg = Color.decode("#666666");
    public static final Color DefaultBg = Color.decode("#333333");
    public static final Color PacmanFg  = Color.YELLOW;
    public static final Color CoinFg    = Color.YELLOW;

    public static final boolean DebugDraws = true;
}
