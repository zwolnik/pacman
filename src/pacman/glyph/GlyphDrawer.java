package pacman.glyph;

import pacman.conf.Config;
import pacman.game.Direction;

import java.awt.*;
import java.io.InputStream;

/**
 * Class GlyphDrawer is responsible for drawing glyphs on the Panel's surface
 *
 * It supports normal drawing at given position as well as drawing interpolated glyphs depending on a time delta
 */
public class GlyphDrawer {
    private Font font;

    public GlyphDrawer() {
        try {
            InputStream is = getClass().getResourceAsStream("/pacman/assets/square.ttf");
            font = Font.createFont(Font.TRUETYPE_FONT, is);
        } catch(Exception e) {
            System.exit(1);
        }
    }

    private int realX(int x) {
        return Config.XOffset + x * Config.XUnit;
    }

    private int realY(int y) {
        return Config.YOffset + y * Config.YUnit;
    }

    private int interpX(int interpolation, Direction dir) {
        int interp = Config.XUnit * interpolation / Config.GameSpeed;
        switch (dir) {
            case LEFT: return -interp;
            case RIGHT: return interp;
            default: return 0;
        }
    }

    private int interpY(int interpolation, Direction dir) {
        int interp = Config.YUnit * interpolation / Config.GameSpeed;
        switch (dir) {
            case UP: return -interp;
            case DOWN: return interp;
            default: return 0;
        }
    }

    public void drawAtInterpolated(
            Graphics g,
            Glyph glyph,
            int x, int y,
            int interpolation,
            Direction dir
    ) {
        draw(g, glyph,
            realX(x) + interpX(interpolation, dir),
            realY(y) + interpY(interpolation, dir));
    }

    public void drawAt(
            Graphics g,
            Glyph glyph,
            int x, int y
    ) {
        draw(g, glyph, realX(x), realY(y));
    }

    public void draw(
            Graphics g,
            Glyph glyph,
            int x, int y
    ) {
        g.setFont(font.deriveFont(22f));
        FontMetrics fm = g.getFontMetrics();

        if (!glyph.transparent) {
            g.setColor(glyph.bg);
            g.fillRect(x,
                    y - fm.getAscent(),
                    Config.XUnit,
                    Config.YUnit);
        }

        g.setColor(glyph.fg);
        g.drawString(Character.toString(glyph.ch), x, y);
    }
}
