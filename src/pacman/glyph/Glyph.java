package pacman.glyph;

import pacman.conf.Config;

import java.awt.*;

/**
 * Class Glyph represents single drawable character
 *
 * It consists of Background color, Foreground (font) color, character to display and boolean if background is transparent
 */
public class Glyph {
    public Color fg;
    public Color bg;
    public char ch;
    public boolean transparent;

    public Glyph(char c) {
        this.fg = Config.DefaultFg;
        this.bg = Config.DefaultBg;
        this.ch = c;
        this.transparent = false;
    }

    public Glyph(char c, Color fg, Color bg) {
        this.fg = fg;
        this.bg = bg;
        this.ch = c;
        this.transparent = false;
    }

    public Glyph negative() {
        return new Glyph(ch, new Color(fg.getRGB() ^ 0x00ffffff), bg);
    }

    public static Glyph Wall(pacman.game.components.Wall.Type type) {
        char ch;
        switch (type) {
            case Vertical:
                ch = '|';
                break;
            case Horizontal:
                ch = '-';
                break;
            default:
                ch = '#';
                break;
        }
        Glyph g = new Glyph(ch);
        g.fg = Config.WallFg;
        g.bg = Config.WallBg;
        return g;
    }

    public static Glyph Ghost(Color ghostColor) {
        Glyph g = new Glyph('0');
        g.fg = ghostColor;
        g.bg = Config.DefaultBg;
        g.transparent = true;
        return g;
    }

    public static Glyph Pacman() {
        Glyph g = new Glyph('@');
        g.fg = Config.PacmanFg;
        g.bg = Config.DefaultBg;
        g.transparent = true;
        return g;
    }

    public static Glyph Coin() {
        Glyph g = new Glyph('.');
        g.fg = Config.CoinFg;
        g.bg = Config.DefaultBg;
        return g;
    }

    public static Glyph Tile() {
        Glyph g = new Glyph(' ');
        g.fg = Config.DefaultFg;
        g.bg = Config.DefaultBg;
        return g;
    }

    public static Glyph Text(char t) {
        Glyph g = new Glyph(t);
        g.fg = Config.CoinFg;
        g.bg = Config.WallBg;
        return g;
    }
}
