package pacman.game;

/**
 * Direction is an enum representing where game objects are heading
 */
public enum Direction {
    UP,
    DOWN,
    LEFT,
    RIGHT;

    Direction turnLeft, turnRight, opposite;

    static {
        UP.turnLeft = LEFT;
        UP.turnRight = RIGHT;
        UP.opposite = DOWN;
        DOWN.turnLeft = RIGHT;
        DOWN.turnRight = LEFT;
        DOWN.opposite = UP;
        LEFT.turnLeft = DOWN;
        LEFT.turnRight = UP;
        LEFT.opposite = RIGHT;
        RIGHT.turnLeft = UP;
        RIGHT.turnRight = DOWN;
        RIGHT.opposite = LEFT;
    }

    public Direction turn(Turn t) {
        switch (t) {
            case LEFT: return turnLeft;
            case RIGHT: return turnRight;
            case STRAIGHT: return this;
        }
        return this;
    }

    public Direction opposite() {
        return opposite;
    }
}
