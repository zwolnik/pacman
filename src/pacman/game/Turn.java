package pacman.game;

/**
 * Turn enum represents possible turns that objects can take in game
 */
public enum Turn {
    LEFT,
    RIGHT,
    STRAIGHT,
}
