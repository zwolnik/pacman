package pacman.game;

import pacman.game.components.*;
import pacman.game.components.ghosts.BlueGhost;
import pacman.game.components.ghosts.GreenGhost;
import pacman.game.components.ghosts.RedGhost;
import pacman.game.components.ghosts.WhiteGhost;

import java.util.ArrayList;
import java.util.Arrays;

/**
 * Class Board contains informations about placements of all components:
 * Walls, Corridors, Coins, SuperCoins, Ghosts, Pacman
 */
public class Board {
    public static final String[] template = {
            "-------------##-------------",
            "|o...........##...........o|",
            "|.####.#####.##.#####.####.|",
            "|.####.#####.##.#####.####.|",
            "|.####.#####.##.#####.####.|",
            "|..........................|",
            "|.####.##.########.##.####.|",
            "|.####.##.########.##.####.|",
            "|......##....##....##......|",
            "|.####.#####.##.#####.#####|",
            "|.#  #.#####.##.#####.#   #|",
            "|.#  #.##..........##.#   #|",
            "|.####.##.# #### #.##.#####|",
            "|......##.# 0000 #.##......|",
            "|#####....########....####.|",
            "|#   #.##..........##.#  #.|",
            "|#   #.##.########.##.#  #.|",
            "|#####.##.########.##.####.|",
            "|............##............|",
            "|.####.#####.##.#####.####.|",
            "|.####.#####.##.#####.####.|",
            "|...##................##...|",
            "|##.##.##.########.##.##.##|",
            "|##.##.##.########.##.##.##|",
            "|......##...@##....##......|",
            "|.##########.##.##########.|",
            "|.##########.##.##########.|",
            "|o........................o|",
            "----------------------------"
    };

    private final Component[][] board;
    public ArrayList<Component> ghosts;
    public Component pacman;

    public Board() {
        int currentGhost = 0;
        board = new Component[template.length][template[0].length()];
        ghosts = new ArrayList<>();
        for (int y=0; y < template.length; ++y) {
            for (int x=0; x < template[0].length(); ++x) {
                switch (template[y].charAt(x)) {
                    case '#':
                        board[y][x] = new Wall(x, y, Wall.Type.Block);
                        break;
                    case '|':
                        board[y][x] = new Wall(x, y, Wall.Type.Vertical);
                        break;
                    case '-':
                        board[y][x] = new Wall(x, y, Wall.Type.Horizontal);
                        break;
                    case '@':
                        board[y][x] = new Tile(x, y);
                        pacman = new Pacman(x, y, this);
                        break;
                    case '0':
                        board[y][x] = new Tile(x, y);
                        switch (currentGhost++) {
                            case 0:
                                ghosts.add(new RedGhost(x, y, this));
                                break;
                            case 1:
                                ghosts.add(new BlueGhost(x, y, this));
                                break;
                            case 2:
                                ghosts.add(new WhiteGhost(x, y, this));
                                break;
                            case 3:
                                ghosts.add(new GreenGhost(x, y, this));
                                break;
                        }
                        break;
                    case '.':
                        board[y][x] = new Coin(x, y);
                        break;
                    case 'o':
                        board[y][x] = new SuperCoin(x, y);
                        break;
                    default:
                        board[y][x] = new Tile(x, y);
                        break;
                }
            }
        }
    }

    public Component[][] get() {
        return board;
    }

    public Component getAt(int x, int y) {
        return board[y][x];
    }

    public Component[][] getWithGhosts() {
        Component[][] ret = new Component[board.length][board[0].length];
        for (int i = 0; i < board.length; ++i) {
            ret[i] = Arrays.copyOf(board[i], board[i].length);
        }
        for (Component ghost : ghosts) {
            ret[ghost.y][ghost.x] = ghost;
        }
        return ret;
    }

    public Coords pacmanLoc() {
        return new Coords(pacman.x, pacman.y);
    }
}
