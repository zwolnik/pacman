package pacman.game;

import pacman.conf.Config;
import pacman.game.components.Component;
import pacman.game.components.Interactive;
import pacman.game.components.Pacman;
import pacman.game.components.ghosts.Ghost;
import pacman.game.components.ghosts.GhostRunnable;
import pacman.glyph.Glyph;
import pacman.glyph.GlyphDrawer;

import java.awt.*;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.io.FileWriter;
import java.io.IOException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.LockSupport;

import javax.swing.JFrame;
import javax.swing.JPanel;

/**
 * Class Window defines Swing's window properties and contains 'main' method - program's entry point
 */
public class Window extends JFrame {
    public Window(Board board) {
        setTitle("Pacman");
        add(new GamePanel(board));
        pack();

        setDefaultCloseOperation(EXIT_ON_CLOSE);
        setLocationRelativeTo(null);
        setResizable(false);
        setVisible(true);
    }

    public static void main(String[] args) {
        EventQueue.invokeLater(() -> new Window(new Board()));
    }

    /**
     * Class GamePanel is responsible for controlling whole game's behaviour
     */
    public class GamePanel extends JPanel {
        int gameCounter;
        int terrifiedCounter;
        public int score;
        boolean running;
        Board board;
        Turn nextTurn;
        GlyphDrawer glyphDrawer;
        ArrayList<Thread> ghosts;
        ScheduledFuture<?> scheduledUpdate;

        /**
         * This class implements runnable interface and is later passed to scheduled executor's thread,
         * from where it controls when update method is called
         */
        class UpdateRunnable implements Runnable {
            GamePanel panel;
            public UpdateRunnable(GamePanel panel) {
                this.panel = panel;
            }
            @Override
            public void run() {
                //System.out.println("Triggering update");
                panel.update();
            }
        }

        public GamePanel(Board board) {
            this.nextTurn = Turn.STRAIGHT;
            this.gameCounter = 0;
            this.terrifiedCounter = 0;
            this.score = 1;
            this.running = true;
            this.board = board;
            this.ghosts = new ArrayList<>();
            this.glyphDrawer = new GlyphDrawer();
            setPreferredSize(new Dimension(Config.WWidth, Config.WHeight));
            setFocusable(true);
            addKeyListener(new GameInput(this));

            ScheduledExecutorService executor  = Executors.newSingleThreadScheduledExecutor();
            scheduledUpdate = executor.scheduleAtFixedRate(new UpdateRunnable(this),
                0, Config.DrawSpeed, TimeUnit.MILLISECONDS);

            for (Component ghost : board.ghosts) {
                Thread ghostRunner = new Thread(new GhostRunnable((Ghost) ghost) {
                    @Override
                    public void run() {
                        while (true) {
                            System.out.println("Parking thread " + Thread.currentThread().getName());
                            LockSupport.park();
                            System.out.println("Moving ghost " + Thread.currentThread().getName());
                            this.ghost.steer();
                        }
                    }
                });
                ghostRunner.setName(ghost.getClass().getSimpleName());
                ghostRunner.start();
                ghosts.add(ghostRunner);
            }
        }

        public void paintComponent(Graphics g) {
            super.paintComponent(g);
            setBackground(Color.decode("#1f1f1f"));
            updateScore();

            for (Component[] row : board.get()) {
                for (Component current : row) {
                    current.draw(g, glyphDrawer, gameCounter);
                }
            }
            for (Component ghost : board.ghosts) {
                ghost.draw(g, glyphDrawer, gameCounter);
            }
            if (running) {
                board.pacman.draw(g, glyphDrawer, gameCounter);
            }
            for (Component[] row : board.get())
                for (Component tile : row)
                    if ("#|-".indexOf(tile.g.ch) == -1)
                        tile.g.bg = Config.DefaultBg;
        }

        public void update() {
            if (!running) {
                return;
            }
            if (nextTurn != Turn.STRAIGHT) {
                ((Pacman) board.pacman).turn(nextTurn);
                nextTurn = Turn.STRAIGHT;
            }

            if (++gameCounter >= Config.GameSpeed) {
                System.out.println("Moving all components");

                ((Pacman) board.pacman).move();
                interact();
                for (Component ghost : board.ghosts) {
                    ((Ghost) ghost).move();
                }
                interact();

                System.out.println("Unparking ghosts");
                for (Thread ghostHandle : ghosts)
                    LockSupport.unpark(ghostHandle);

                gameCounter = 0;

                if (--terrifiedCounter == 0) {
                    for (Component ghost : board.ghosts)
                        ((Ghost) ghost).unterrify();
                }
            }
            repaint();
        }

        private void interact() {
            Component atPacmanLocation = board.getWithGhosts()[board.pacman.y][board.pacman.x];
            if (atPacmanLocation instanceof Interactive)
                ((Interactive) atPacmanLocation).interact(this);
        }

        /**
         * GameInput class is responsible for controlling user provided inputs from keyboard
         */
        private class GameInput extends KeyAdapter {
            private final GamePanel panel;

            public GameInput(GamePanel panel) {
                this.panel = panel;
            }

            @Override
            public void keyPressed(KeyEvent e) {
                if (e.getKeyCode() == KeyEvent.VK_LEFT) {
                    panel.nextTurn = Turn.LEFT;
                }
                if (e.getKeyCode() == KeyEvent.VK_RIGHT) {
                    panel.nextTurn = Turn.RIGHT;
                }
                if (e.getKeyCode() == KeyEvent.VK_S) {
                    try {
                        FileWriter myWriter = new FileWriter("score.txt");
                        myWriter.write("[" + new Timestamp(System.currentTimeMillis()) + "] Score: " + panel.score);
                        myWriter.close();
                        System.out.println("Successfully wrote score to the file.");
                    } catch (IOException ex) {
                        System.out.println("An error occurred.");
                        ex.printStackTrace();
                    }
                }
            }
        }

        public void gameOver() {
            running = false;
            String message = "GameOver";
            Component[] last_row = board.get()[board.get().length-1];
            for (int i = 0; i < message.length(); ++i) {
                last_row[i].g = Glyph.Text(message.charAt(i));
            }
        }

        public void addPoints(int n) {
            score += n;
        }

        private void updateScore() {
            String current_score = "Score " + score;
            Component[] last_row = board.get()[board.get().length-1];
            for (int i = 1; i <= current_score.length(); ++i) {
                last_row[last_row.length - i].g = Glyph.Text(current_score.charAt(current_score.length()-i));
            }
        }

        public void terrify() {
            terrifiedCounter = 20;
            for (Component ghost : board.ghosts)
                ((Ghost) ghost).terrify();
        }
    }
}
