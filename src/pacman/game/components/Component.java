package pacman.game.components;

import pacman.glyph.Glyph;
import pacman.glyph.GlyphDrawer;

import java.awt.*;

/**
 * Generic in game component class, it contains position and graphical representation of in game object
 */
public class Component implements Drawable {
    public int x;
    public int y;
    public Glyph g;

    public Component(int x, int y, Glyph g) {
        this.x = x;
        this.y = y;
        this.g = g;
    }

    public Coords getCoords() {
        return new Coords(x, y);
    }

    @Override
    public void draw(Graphics graphics, GlyphDrawer drawer, int interpolation) {
        drawer.drawAt(graphics, g, x, y);
    }
}
