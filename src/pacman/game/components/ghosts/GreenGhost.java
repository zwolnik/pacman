package pacman.game.components.ghosts;

import pacman.conf.Config;
import pacman.game.Board;
import pacman.game.Direction;
import pacman.game.components.Pacman;

import java.awt.*;

/**
 * Green ghost is a ghost that randomly choose behaviour of Red and Blue ghost with a random strength
 */
public class GreenGhost extends Ghost {
    public GreenGhost(int x, int y, Board b) {
        super(x, y, b, Color.GREEN);
    }

    @Override
    public void move() {
        super.move();
        Direction predictDir = Math.random() > 0.5
            ? ((Pacman) board.pacman).direction
            : ((Pacman) board.pacman).direction.opposite();
        int predictAmount = (int) (Math.random() * 5) + 1;
        target = predict(board.pacman.x, board.pacman.y, predictDir, predictAmount);
        if (Config.DebugDraws)
            board.get()[target.y][target.x].g.bg = color;
    }
}
