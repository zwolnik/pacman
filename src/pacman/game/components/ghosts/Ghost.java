package pacman.game.components.ghosts;

import pacman.game.Board;
import pacman.game.Direction;
import pacman.game.Turn;
import pacman.game.Window.GamePanel;
import pacman.game.components.Coords;
import pacman.game.components.Interactive;
import pacman.game.components.MovableComponent;
import pacman.glyph.Glyph;

import java.awt.*;
import java.util.*;

/**
 * A generic Ghost class, it contains all the logic related to ghosts.
 * The ghost's role is to hunt player
 * It has an interface for predicting Player's future position, has logic regarding terrified state of ghosts,
 * as well as countdown before beginning of hunting the player
 */
public abstract class Ghost extends MovableComponent implements Interactive {
    protected boolean terrified;
    protected Color color;
    protected int initX, initY;
    protected int huntingCountdown;

    public Coords target;

    public Ghost(int x, int y, Board b, Color c) {
        super(x, y, Glyph.Ghost(c), b);
        color = c;
        terrified = false;
        huntingCountdown = 10;
        target = new Coords(x, y);
        initX = x;
        initY = y;
    }

    @Override
    public void move() {
        super.move();
        target = board.pacmanLoc();
    }

    @Override
    public void interact(GamePanel panel) {
        if (!terrified)
            panel.gameOver();
        else {
            panel.addPoints(10);
            x = initX;
            y = initY;
            direction = Direction.LEFT;
            unterrify();
        }
    }

    public void terrify() {
        terrified = true;
        nextDirection = direction.opposite();
        nextBlocked = isBlocked(x, y, nextDirection);
        g = g.negative();
    }

    public void unterrify() {
        terrified = false;
        g.fg = color;
    }

    protected class Move {
        public Coords coords;
        public Direction dir;
        public Turn turn;

        public Move(int x, int y, Direction dir, Turn turn) {
            this.coords = new Coords(x, y);
            this.dir = dir;
            this.turn = turn;
        }

        public Coords execute() {
            return advance(coords.x, coords.y, dir);
        }
    }

    public void steer() {
        Coords next = getCoords();
        if (!blocked) {
            next = advance(x, y, direction);
        }
        if (next.distance(board.pacmanLoc()) <= 3) {
            target = board.pacmanLoc();
        }
        if (!terrified && --huntingCountdown < 0) { // BFS
            Set<Coords> seen = new HashSet<>();
            HashMap<Coords, Move> mapping = new HashMap<>();
            ArrayList<Move> current = new ArrayList<>();
            boolean found = false;

            seen.add(next);
            for (Move m : availableMoves(next.x, next.y, direction)) {
                current.add(m);
                mapping.put(m.execute(), m);
            }

            if (next.distance(board.pacmanLoc()) < 8)
                target = board.pacmanLoc();

            bfs_loop:
            while (current.size() > 0) {
                ArrayList<Move> nextChecking = new ArrayList<>();
                for (Move currentMove : current) {
                    Coords newCoords = currentMove.execute();
                    if (seen.add(newCoords)) {
                        if (newCoords.x == target.x && newCoords.y == target.y) {
                            mapping.put(target, currentMove);
                            found = true;
                            break bfs_loop;
                        }
                        mapping.put(newCoords, currentMove);
                        nextChecking.addAll(availableMoves(newCoords.x, newCoords.y, currentMove.dir));
                    }
                }
                current = nextChecking;
            }
            if (found) {
                Move backtrack = mapping.get(target);
                while (true) {
                    if (mapping.get(backtrack.coords) != null)
                        backtrack = mapping.get(backtrack.coords);
                    else
                        break;
                }
                turn(backtrack.turn);
                return;
            }
        } // terrified: move randomly
        turn(randomTurn(next.x, next.y, direction));
    }

    public ArrayList<Move> availableMoves(int x, int y, Direction dir) {
        ArrayList<Move> ret = new ArrayList<>();
        for (Turn t : Arrays.asList(Turn.LEFT, Turn.RIGHT, Turn.STRAIGHT)) {
            if (!isBlocked(x, y, dir.turn(t))) {
                ret.add(new Move(x, y, dir.turn(t), t));
            }
        }
        return ret;
    }

    protected Turn randomTurn(int x, int y, Direction dir) {
        ArrayList<Move> moves = availableMoves(x, y, dir);
        if (moves.size() > 0) {
            return moves.get((int) (Math.random() * moves.size())).turn;
        }
        return Turn.STRAIGHT;
    }

    protected Coords predict(int x, int y, Direction dir, int movesAmount) {
        Coords ret = new Coords(x, y);
        for (int i = 0; i < movesAmount; ++i) {
            ArrayList<Move> moves = availableMoves(ret.x, ret.y, dir);
            if (!moves.isEmpty()) {
                int randomIdx = (int) (Math.random() * moves.size());
                dir = moves.get(randomIdx).dir;
            } else
                break;
            ret = advance(ret.x, ret.y, dir);
        }
        return ret;
    }
}
