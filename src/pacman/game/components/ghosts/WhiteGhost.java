package pacman.game.components.ghosts;

import pacman.game.Board;

import java.awt.*;

/**
 * White ghost is the ghost that moves randomly across the board
 * It is in fact in terrified state all the time, terrifying it by collecting SuperCoin
 * has only visual effect and turns the ghosts in opposite direction
 */
public class WhiteGhost extends Ghost {
    public WhiteGhost(int x, int y, Board b) {
        super(x, y, b, Color.WHITE);
        terrified = true;
    }

    @Override
    public void unterrify() {
        super.unterrify();
        terrified = true;
    }
}
