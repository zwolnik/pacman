package pacman.game.components.ghosts;

import pacman.conf.Config;
import pacman.game.Board;
import pacman.game.components.MovableComponent;

import java.awt.*;

/**
 * Red ghost is a ghost that tries to surprise pacman by jumping in front of him
 */
public class RedGhost extends Ghost {
    public RedGhost(int x, int y, Board b) {
        super(x, y, b, Color.RED);
    }

    @Override
    public void move() {
        super.move();
        target = predict(
                board.pacman.x, board.pacman.y,
                ((MovableComponent) board.pacman).direction,
                5);
        if (Config.DebugDraws)
            board.get()[target.y][target.x].g.bg = color;
    }
}
