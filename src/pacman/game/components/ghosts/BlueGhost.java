package pacman.game.components.ghosts;

import pacman.conf.Config;
import pacman.game.Board;
import pacman.game.components.MovableComponent;

import java.awt.*;

/**
 * Blue ghost is the ghost that tries to cut out Player's escape routes
 */
public class BlueGhost extends Ghost {
    public BlueGhost(int x, int y, Board b) {
        super(x, y, b, Color.BLUE);
    }

    @Override
    public void move() {
        super.move();
        target = predict(
                board.pacman.x, board.pacman.y,
                ((MovableComponent) board.pacman).direction.opposite(),
                5);
        if (Config.DebugDraws)
            board.get()[target.y][target.x].g.bg = color;
    }
}
