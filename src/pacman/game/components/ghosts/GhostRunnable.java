package pacman.game.components.ghosts;

/**
 * A Runnable that has a reference to a ghost it controls
 */
public abstract class GhostRunnable implements Runnable {
    public Ghost ghost;
    public GhostRunnable(Ghost ghost) {
        this.ghost = ghost;
    }
}
