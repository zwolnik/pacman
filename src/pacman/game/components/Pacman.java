package pacman.game.components;

import pacman.game.Board;
import pacman.game.Direction;
import pacman.game.Turn;
import pacman.glyph.Glyph;

/**
 * Class Pacman represents a Pacman
 */
public class Pacman extends MovableComponent {
    public Pacman(int x, int y, Board b) {
        super(x, y, Glyph.Pacman(), b);
        direction = Direction.LEFT;
    }

    @Override
    public void turn(Turn t) {
        super.turn(t);
        if (nextBlocked) {
            nextDirection = direction;
            nextBlocked = blocked;
        }
    }
}
