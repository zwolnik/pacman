package pacman.game.components;

import pacman.glyph.Glyph;

/**
 * Component representing wall on board
 */
public class Wall extends Component {
    public enum Type {
        Block,
        Vertical,
        Horizontal,
    }

    public Wall(int x, int y, Type type) {
        super(x, y, Glyph.Wall(type));
    }
}
