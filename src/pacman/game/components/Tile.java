package pacman.game.components;

import pacman.glyph.Glyph;

/**
 * Class Tile represents an empty Tile on a board
 */
public class Tile extends Component {
    public Tile(int x, int y) {
        super(x, y, Glyph.Tile());
    }
}
