package pacman.game.components;

import pacman.game.Window.GamePanel;

/**
 * An interface representing all objects with an ability to interact with Pacman
 */
public interface Interactive {
    void interact(GamePanel panel);
}
