package pacman.game.components;

import pacman.game.Window.GamePanel;
import pacman.glyph.Glyph;

/**
 * Simple coin that gives Player points upon collection
 */
public class Coin extends Component implements Interactive {
    public Coin(int x, int y) {
        super(x, y, Glyph.Coin());
    }

    @Override
    public void interact(GamePanel panel) {
        if (g.ch == '.') {
            g.ch = ' ';
            panel.addPoints(1);
        }
    }
}
