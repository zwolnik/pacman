package pacman.game.components;

import pacman.game.Window;

/**
 * Class SuperCoin represents a Coin that is 5 times more worthy than regular coin, and applies terrification effect on ghosts
 */
public class SuperCoin extends Coin {
    public SuperCoin(int x, int y) {
        super(x, y);
        this.g.ch = 'o';
    }

    @Override
    public void interact(Window.GamePanel panel) {
        if (g.ch == 'o') {
            panel.addPoints(5);
            panel.terrify();
            g.ch = ' ';
        }
    }
}
