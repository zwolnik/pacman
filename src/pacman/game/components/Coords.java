package pacman.game.components;

/**
 * Class that represents position on board. It can be compared with other coords, and used as a key in hashmap
 */
public class Coords implements Comparable<Coords> {
    public int x;
    public int y;

    public Coords(int x, int y) {
        this.x = x;
        this.y = y;
    }

    public int distance(Coords other) {
        return (int) Math.sqrt(
                Math.pow(this.x - other.x, 2) +
                Math.pow(this.y - other.y, 2));
    }

    @Override
    public boolean equals(Object obj){
        if (this == obj) return true;
        if (!(obj instanceof Coords)) return false;

        Coords that = (Coords)obj;
        return this.x == that.x && this.y == that.y;
    }

    @Override
    public int hashCode(){
        return x * 31 + y;
    }

    @Override
    public int compareTo(Coords that){
        if (this.equals(that))
            return 0;
        else if (this.x < that.x)
            return -1;
        else if (this.x > that.x)
            return 1;
        else if (this.y < that.y)
            return -1;
        else
            return 1;
    }
}
