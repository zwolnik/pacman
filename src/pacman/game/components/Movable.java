package pacman.game.components;

/**
 * An interface representing any movable object
 */
public interface Movable {
    void move();
}
