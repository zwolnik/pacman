package pacman.game.components;

import pacman.glyph.GlyphDrawer;

import java.awt.*;

/**
 * An interface that represents all visible objects
 */
public interface Drawable {
    void draw(Graphics g, GlyphDrawer drawer, int interpolation);
}
