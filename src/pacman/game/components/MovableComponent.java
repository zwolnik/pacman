package pacman.game.components;

import pacman.game.Board;
import pacman.game.Direction;
import pacman.game.Turn;
import pacman.glyph.Glyph;
import pacman.glyph.GlyphDrawer;

import java.awt.*;

/**
 * Movable Component contain unified logic for all game components with an ability to move
 * It defines how turns are taken, aswell implements logic that manages objects collisions
 */
public class MovableComponent extends Component implements Movable {
    protected Board board;
    public Direction direction;
    public Direction nextDirection;
    public boolean blocked;
    public boolean nextBlocked;

    public MovableComponent(int x, int y, Glyph g, Board b) {
        super(x, y, g);
        board = b;
        direction = Direction.LEFT;
        nextDirection = Direction.LEFT;
        blocked = isBlocked(x, y, direction);
        nextBlocked = blocked;
    }

    public Coords advance(int x, int y, Direction dir) {
        switch (dir) {
            case UP:
                y -= 1;
                break;
            case DOWN:
                y += 1;
                break;
            case LEFT:
                x -= 1;
                break;
            case RIGHT:
                x += 1;
                break;
        }
        return new Coords(x, y);
    }

    public boolean isBlocked(int x, int y, Direction dir) {
        Coords next = advance(x, y, dir);
        Component nextTile = board.getAt(next.x, next.y);
        return !(nextTile instanceof Tile || nextTile instanceof Coin);
    }

    public void turn(Turn t) {
        if (t != Turn.STRAIGHT) {
            nextDirection = direction.turn(t);
            Coords next = new Coords(x, y);
            if (!blocked) {
                next = advance(x, y, direction);
            }
            nextBlocked = isBlocked(next.x, next.y, nextDirection);
        }
    }

    @Override
    public void move() {
        if (nextBlocked && blocked)
            return;
        Coords next = new Coords(x, y);
        if (!blocked) {
            next = advance(x, y, direction);
        }
        x = next.x;
        y = next.y;
        direction = nextDirection;
        blocked = isBlocked(x, y, direction);
        nextBlocked = blocked;
    }

    @Override
    public void draw(Graphics graphics, GlyphDrawer drawer, int interpolation) {
        if (!blocked) {
            drawer.drawAtInterpolated(graphics, g, x, y, interpolation, direction);
        }
        else
            drawer.drawAt(graphics, g, x, y);
    }
}
